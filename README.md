CaesarCipher
============

CSCI 469 Project 1 (Fall 13) - Caesar Cipher

How to Run
----------
Execute the jar in the dist/ directory using "java -jar dist/CaesarCipher.jar".

Examples of Input/Output
------------------------
Examples of plaintext and the resulting ciphertext can be found in the sampleIO/
directory.
